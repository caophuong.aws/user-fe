import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../model/user';

@Injectable({
    providedIn: 'root'
})

export class UserService {
    constructor(private http: HttpClient) {

    }

    getList() {
        return this.http.get(`${env.apiUrl}/api/user`);
    }

    addUser(userData: User) {
        let body =  userData ;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem("currentUser")).token
            })
        };

        return this.http.post<any>(`${env.apiUrl}/api/user/`, body, httpOptions).pipe(map(res => {
            if (res.success == true) {
                console.log('currentUser', JSON.stringify(res));
                return res;
            }
        }));
    }

    editUser(userData: User) {
        let body =  userData ;
        let { id } = userData;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem("currentUser")).token
            })
        };

        return this.http.post<any>(`${env.apiUrl}/api/user/`+id, body, httpOptions).pipe(map(res => {
            if (res.success == true) {
                console.log('currentUser', JSON.stringify(res));
                return res;
            }
        }));
    }

    removeUser(userData: User) {
        let { id } = userData;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem("currentUser")).token
            })
        };

        return this.http.delete<any>(`${env.apiUrl}/api/user/`+ id, httpOptions).pipe(map(res => {
            if (res.success == true) {
                return res;
            }
        }));
    }
}