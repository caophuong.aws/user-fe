import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../model/user';
import { animate, state, style, transition, trigger } from '@angular/animations';
import * as lodash from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { AddDialog } from '../directives/add-dialog/add-dialog.directive';
import { EditDialog } from '../directives/edit-dialog/edit-dialog.directive';
import { first } from 'rxjs/operators';
import { UserService } from '../service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class UserComponent implements OnInit {
  lodash = lodash;
  apiRespone: any = {};
  selectedUser: User;
  userList: User[];
  mainColumnsToDisplay: any[] = ['username', 'email', 'firstName', 'lastName', 'phoneNumber', 'action'];
  dataSource;

  expandedElement: User | null;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.fecthUserList();
  }

  private fecthUserList() {
    this.userService.getList()
      .pipe(first())
      .subscribe(
        data => {
          if (data) {
            this.apiRespone = data;
            this.userList = this.apiRespone.results;
            this.dataSource = new MatTableDataSource(this.userList);
          }
        },
        error => {
          this._snackBar.open(
            'Connect server failed',
            'Okay', { duration: 5000 });
        }
      )
  }

  logOut() {
    this.authService.logout();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  actionChange(event) {
    event.stopPropagation();
  }

  editUser(selectedItem: any) {
    if (selectedItem) {
      this.selectedUser = selectedItem;
      this.openEditDialog(this.selectedUser);
    }
  }

  openEditDialog(userData: User): void {
    const dialogRef = this.dialog.open(EditDialog, {
      data: { userData }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.userService.editUser(result.userData)
        .pipe(first())
        .subscribe(
          data => {
            if (data) {
              this.fecthUserList();
            }
          },
          error => {
            this._snackBar.open(
              'Connect server failed',
              'Okay', { duration: 5000 });
          }
        )
    });
  }

  removeUser(selectedItem: any) {
    if (selectedItem) {
      this.userService.removeUser(selectedItem)
      .pipe(first())
      .subscribe(
        data => {
          this.fecthUserList();
        },
        error => {
          this._snackBar.open(
            'Remove user failed',
            'Okay', { duration: 5000 });
        }
      )
    }
  }

  addUser() {
    let newUser: User;
    const dialogRef = this.dialog.open(AddDialog, { data: {newUser}});

    dialogRef.afterClosed().subscribe(resultNewUser => {
      if( resultNewUser != undefined) {
        this.userService.addUser(resultNewUser)
          .pipe(first())
          .subscribe(
            data => {
              this.fecthUserList();
            },
            error => {
              this._snackBar.open(
                'Add user failed',
                'Okay', { duration: 5000 });
            }
          )
      }
    });
  }

}