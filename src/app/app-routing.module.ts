import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// Components
import {LoginComponent} from './login/login.component'
import {UserComponent} from './user/user.component'
import {PageNotFoundComponent} from './page-not-found/page-not-found.component'
import { AuthGuard } from './service/auth.guard';

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
