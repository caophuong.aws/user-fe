export class User {
    id: number;
    email: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    dob: string;
    desc: string;
    // profileImage: string;
    createdBy: string;
    createdAt: string;
    updatedBy: string;
    updatedAt: string
}