import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'edit-dialog',
    templateUrl: './edit-dialog.directive.html',
    styleUrls: ['./edit-dialog.directive.css'],
})
export class EditDialog {
    editUser: User;
    userBirthday;

    constructor(
        public dialogRef: MatDialogRef<EditDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private datePipe: DatePipe) {
            this.editUser = data.userData;
            this.userBirthday = new FormControl(new Date(this.editUser.dob));
    }

    save(): void {
        this.editUser.dob = this.datePipe.transform(this.userBirthday.value, 'yyyy-MM-dd HH:mm:sss');
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}