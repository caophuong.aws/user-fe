import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'add-dialog',
    templateUrl: './add-dialog.directive.html',
    styleUrls: ['./add-dialog.directive.css'],
})
export class AddDialog implements OnInit {
    addUser: User;
    registerForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<AddDialog>,
        @Inject(MAT_DIALOG_DATA) public newUser: User,
        private formBuilder: FormBuilder,
        private datePipe: DatePipe) {
        this.addUser = newUser;
    }

    ngOnInit(): void {
        this.registerForm = this.formBuilder.group({
            registerEmail: new FormControl(null, [ Validators.email, Validators.required]),
            registerUsername: new FormControl(null, [ Validators.required]),
            registerPassword: new FormControl(null, [ Validators.required]),
            registerPhone: new FormControl(null, [ Validators.pattern("^[0-9]*$")]),
            registerBirthday: new FormControl(null),
            registerFirstName: new FormControl(null),
            registerLastName: new FormControl(null),
            registerDesc: new FormControl(null),
        });
    }

    create(): void {
        if (this.registerForm.invalid) {
            return;
        }
        this.addUser.email = this.registerForm.controls['registerEmail'].value;
        this.addUser.username = this.registerForm.controls['registerUsername'].value;
        this.addUser.password = this.registerForm.controls['registerPassword'].value;
        this.addUser.phoneNumber = this.registerForm.controls['registerPhone'].value;
        this.addUser.dob = this.datePipe.transform(this.registerForm.controls['registerBirthday'].value, 'yyyy-MM-dd HH:mm:sss');
        this.addUser.firstName = this.registerForm.controls['registerFirstName'].value;
        this.addUser.lastName = this.registerForm.controls['registerLastName'].value;
        this.addUser.desc = this.registerForm.controls['registerDesc'].value;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}